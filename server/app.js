
const express = require('express');

if (process.env.NODE_ENV === 'development') {
    require('dotenv').config();
}

/* Route declaration */
let userRouter = require('./routes/user')


let app = express();

app.use(express.json());


/* Routes */
app.use('/user', userRouter);



/* Catch 404 and forward to error handler */
app.use(function (req, res, next) {
    next(createError(404));
});


/* Error handler */
app.use(function (err, req, res, next) {

    /* Eet locals, only providing error in development */
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    /* Render the error page */
    res.status(err.status || 500);
    res.json({ error: 'error' });
});

module.exports = app;
