module.exports.addResponseHeaders = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Methods', 'OPTIONS,POST,GET,PUT,DELETE');
    res.header('Allow', 'OPTIONS,POST,GET,PUT,DELETE');
    res.header('Access-Control-Allow-Headers',
        'Content-Type, Authorization, Content-Length, X-Requested-With, Access-Control-Allow-Origin');
    next();
};
