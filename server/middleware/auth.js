let firebaseAdmin = require('../init/firebase');

module.exports.ensureCurrentUser = (req, res, next) => {
    let authToken = req.header('authorization');

    if (req.method === 'OPTIONS') {
        return next()
    }

    /* Check for authorization Bearer JWT */
    if (!authToken || !authToken.startsWith('Bearer ')) {
        res.status(401).send('Authentication required.')
    }

    let jwt = authToken.split(" ")[1];
    
    // Decode JWT
    firebaseAdmin
        .auth()
        .verifyIdToken(jwt)
        .then((decodedToken) => {
            req.currentUser = decodedToken;
            next();
        })
        .catch((error) => {
            res.status(401).send('Authentication required.')
        });
};