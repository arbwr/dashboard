const firebaseAdmin = require('./firebase')

/* Initialize firestore */
module.exports = firestore = firebaseAdmin.firestore();