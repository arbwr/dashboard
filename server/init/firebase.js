
const admin = require('firebase-admin');

/* Get service account data */
const serviceAccount = require('../dashboard-imb-firebase-adminsdk-1x8tg-f969607bec.json')


/* Initialize Firebase */
module.exports = firebaseAdmin = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://dashboard-imb.firebaseio.com',
})