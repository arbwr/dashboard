const express = require('express');
const router = express.Router();
const firebaseAdmin = require('../init/firebase')
const firestore = require('../init/firestore');

/* User function for the database */
const { userSignup } = require('../database/user')

/* Middlewares */
const { ensureCurrentUser } = require('../middleware/auth');
const { addResponseHeaders } = require('../middleware/cors');


/* Bearer Authentication */
// router.use(ensureCurrentUser);
router.use(addResponseHeaders);


router.post('/signup', (req, res) => {

    if (req.method == 'POST') {

        let name = req.body.name
        let email = req.body.email
        let password = req.body.password

        /* Creates user */
        firebaseAdmin
            .auth()
            .createUser({
                email: email,
                emailVerified: false,
                password: password,
                displayName: name,
                disabled: false,
            })
            .then((userRecord) => {
                // See the UserRecord reference doc for the contents of userRecord
                console.log('Successfully created new user:', userRecord.uid);

                // Push the new use into Firestore Database
                userSignup({
                    email: email,
                    username: name,
                    uid: userRecord.uid
                }).then(data => {
                    res.json({ ...data, success: true });
                }).catch(err => {
                    res.json(err);
                })

            })
            .catch((error) => {
                res.json({ message: error.message, code: error.code })
            });
    }
    else if (req.method == 'OPTIONS') {
        res.status(200).end()
    }
    else {
        res.status(403).end()
    }
})



router.post('/login', (req, res) => {

    if (req.method == 'POST') {


        let email = req.body.email
        let password = req.body.password

        if (email && password) {
            /* Creates user */
            firebaseAdmin
                .auth()
                .signInWithEmailAndPassword({
                    email: email,
                    password: password,
                })
                .then((user) => {
                    // See the UserRecord reference doc for the contents of userRecord
                    console.log('Successfully logged in user.');

                    res.json({ 
                        success: true,
                        usernam : user.displayName
                    })
                })
                .catch((error) => {
                    res.json({ message: error.message, code: error.code })
                });
        }
        else {
            res.json({ message: "Insufficient data." })
        }


    }
    else if (req.method == 'OPTIONS') {
        res.status(200).end()
    }
    else {
        res.status(403).end()
    }
})

module.exports = router;