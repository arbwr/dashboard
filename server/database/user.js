const firestore = require('../init/firestore');
const db = firestore.collection("users")

module.exports.userSignup = (data) => {

    return new Promise((resolve, reject) => {
        // Push the new user in Firestore Database
        db.add(data).then(doc => {
            resolve({
                result: `User with doc ID: ${userRecord.uid} added to database.`,
                uid: userRecord.uid,
                docid: doc.id
            });
        }).catch(err => {
            reject({
                result: `User with uid: ${userRecord.uid} created but failed to be added to database.`,
                uid: userRecord.uid,
                error: err
            });
        })
    })
}