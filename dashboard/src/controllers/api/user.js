
const API_PATH = 'http://localhost:4000/user';


export async function userSignup(data) {

    const response = await fetch(`${API_PATH}/signup`, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });

    return response.json();
}