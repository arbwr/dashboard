import {
    API_KEY,
    APP_ID,
    AUTH_DOMAIN,
    PROJECT_ID,
} from "../constants/constants";

let firebase = require("firebase/app");

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: API_KEY,
    authDomain: AUTH_DOMAIN,
    projectId: PROJECT_ID,
    appId: APP_ID
};

// Initialize Firebase
export default firebase.initializeApp(firebaseConfig);