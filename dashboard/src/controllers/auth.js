import firebaseApp from "./init/firebase";
import { getAuth, signInWithEmailAndPassword  } from "firebase/auth"


export function userLogin(email, password) {
    return new Promise((resolve, reject) => {
        const auth = getAuth(firebaseApp);
        signInWithEmailAndPassword(auth, email, password)
            .then(user => {
                resolve(user);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}