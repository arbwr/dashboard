import React, { useState } from 'react'
import 'materialize-css/dist/css/materialize.css'
import 'materialize-css/dist/js/materialize.js'
import DashboardLogo from '../../images/dashboard-logo.png'
import { userSignup } from '../../controllers/api/user'

import './signup.css'


const Signup = () => {

    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const signup = (e) => {
        e.preventDefault();

        let userObject = {
            name,
            email,
            password
        }


        if (email !== "" || password !== "" || name !== "") {
            userSignup(userObject).then(data => {
                console.log(data)
            }).catch(err => {
                console.log(err)
            })
        }

    }

    return (
        <div className="root">
            <div className="main">
                <img src={DashboardLogo} className="dashboard-logo" alt="Dashboard logo" />
                <h1 className="bigText">IMB Dashboards</h1>
            </div>
            <div className="signupForm">
                <form className="col hoverable">
                    <div className="row">
                        <div className="col s12">
                            <h4 className="center">
                                Sign up
                            </h4>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" className="name" id="displayname" value={name} onChange={(e) => { setName(e.target.value) }} required />
                            <label htmlFor="displayname">Name</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="email" className="email validate" id="signup-email" value={email} onChange={(e) => { setEmail(e.target.value) }} required />
                            <label htmlFor="signup-email">Email</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="password" className="password validate" id="password" value={password} onChange={(e) => { setPassword(e.target.value) }} required />
                            <label htmlFor="password">Password</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12 center">
                            <button className="btn btn-waves-effect waves-light btn btn-large submit-signup" id="submit" onClick={signup}>Go!</button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12 center">
                            <span id="existingacc">Already a member?</span>
                            <a href="/login" className="existingacc test">Log in</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Signup
