import React, { useState } from 'react'
import 'materialize-css/dist/css/materialize.css'
import 'materialize-css/dist/js/materialize.js'
import DashboardLogo from '../../images/dashboard-logo.png'
import { userLogin } from '../../controllers/auth'

import './login.css'

const Login = () => {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")


    const login = (e) => {
        e.preventDefault();

        if (email !== "" || password !== "") {
            userLogin(email, password).then(data => {
                console.log(data.user)
                window.location = '/'
            }).catch(err => {
                console.log(err)
            })
        }
    }


    return (
        <div className="root">
            <div className="main">
                <img src={DashboardLogo} className="dashboard-logo" alt="Dashboard logo" />
                <h1 className="bigText">IMB Dashboards</h1>
            </div>
            <div className="signupForm">
                <form className="col hoverable">
                    <div className="row">
                        <div className="col s12">
                            <h4 className="center">
                                Log in
                            </h4>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="email" className="email validate" id="signup-email" value={email} onChange={(e) => { setEmail(e.target.value) }} required />
                            <label htmlFor="signup-email">Email</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="password" className="password validate" id="password" value={password} onChange={(e) => { setPassword(e.target.value) }} required />
                            <label htmlFor="password">Password</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12 center">
                            <button className="btn btn-waves-effect waves-light btn btn-large submit-login" id="submit" onClick={login}>Go!</button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12 center">
                            <span id="existingacc">Don't have an account?</span>
                            <a href="/signup" className="existingacc test">Sign up</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    )
}

export default Login
