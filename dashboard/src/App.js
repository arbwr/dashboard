import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import Signup from './components/Signup/Signup';
import Login from './components/Login/Login'
import { BrowserRouter as Router, Route } from "react-router-dom"

function App() {
  return (
    <Router> 
        <Route path="/signup" exact component={Signup} />
        <Route path="/login" component={Login} />
    </Router>

  );
}

export default App;
